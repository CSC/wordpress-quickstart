<?php
/*
Plugin Name: CERN SSO Authentication
Plugin URI: http://gitlab.cern.ch/wordpress/cern-sso-authentication/
Description: Adds Authentication by using CERN SSO information from the HTTP header X-Remote-User, send by Apache proxy
Version: 1.0
Author: Emmanuel Ormancey
Author URI: http://gitlab.cern.ch/ormancey/
License: GPLv3
*/

/*
Stuff here: https://usersinsights.com/wordpress-user-login-hooks/
https://wordpress.stackexchange.com/questions/225544/integrating-wordpress-to-my-website-while-keeping-my-own-authentication-system

*/

// Namespace
//namespace WP_Basic_Auth;
//namespace CERN_SSO_Authentication;

// Updater from https://github.com/YahnisElsts/plugin-update-checker
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = new Puc_v4p6_Vcs_PluginUpdateChecker(
    new Puc_v4p6_Vcs_GitLabApi('https://gitlab.cern.ch/wordpress/cern-sso-authentication/'),
    __FILE__,
    'cern-sso-authentication'
);
//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('stable-release');


// Security check
defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

require_once 'wp-includes/capabilities.php';
//require_once 'wp-includes/class-wp-user.php';

// Function to authenticate the user
//remove_filter( 'authenticate', 'wp_authenticate_username_password', 20);
add_filter( 'authenticate', //'plugins_loaded',
	function() {

		// Username
		$email     = false;
		if ( isset( $_SERVER['HTTP_X_REMOTE_USER'] ) ) {
			$email = $_SERVER['HTTP_X_REMOTE_USER'];
		}

		// We have to authenticate this user
		if ( $email ) {
			$user = WP_User::get_data_by( "email", $email );
			// Found in DB
			if ( $user ) {
				//return $user;
				return new WP_User($user->ID);
			}
			
			$errorMessage = "<h2>Admin access unauthorized</h2>";
			$errorMessage = $errorMessage . "<br/>Access is restricted, please contact this site owner if you need an authorization.";
			$errorMessage = $errorMessage . "<p>User <strong>" . $email . "</strong> not found in authorized user list.</p>";

			return new WP_Error('User not found in WP DB', __($errorMessage));
			//return new WP_Error('User not found in WP DB', __("User not found in Wordpress User list: " . $email));
			//return false;

		} else {

			$errorMessage = "<h2>General error</h2>";
			$errorMessage = $errorMessage . "<br/>Error mapping CERN SSO attributes, either the CERN SSO did not pass the appropriate attributes, or you attempted an unsupported operation.";
			return new WP_Error('Missing SSO Mail Attribute', __($errorMessage));
			//return new WP_Error('error mapping CERN SSO attributes ', __('HTTP_X_REMOTE_USER not set'));
		}

	}, 20, 3
);

add_action( 'login_form',
 	function() {
		// echo '' . __('Login with Custom', 'custom_login') . '
		// ';
		// https://codex.wordpress.org/Plugin_API/Action_Reference/login_form
		$lf_content = ob_get_contents();
		//$lf_content = preg_replace( '/\<p id="nav"\>(.*?)\<\/p\>/ms', '', $lf_content ); // Remove forgot password link /ms options to multiline the search
		//$lf_content = preg_replace( '/\<form id="loginform"(.*?)\<\/form\>/ms', 'FORM REMOVED', $lf_content ); // remove login form /ms options to multiline the search
		
		$doc = new DOMDocument;
		$doc->loadHTML($lf_content);

		foreach ($doc->getElementsByTagName('form') as $tag) {
			$tag->parentNode->removeChild($tag);
		}
		//      foreach ($doc->getElementsByTagName('p') as $tag) {
        //              if ($tag->getAttribute('id') === 'submit')
        //                      $tag->parentNode->removeChild($tag);
		//      }
		
		$lf_content = $doc->saveHTML();

		ob_get_clean();
		echo $lf_content;
	}
);

// Hide footer links and useless links
add_action( 'login_head', 'hide_login_nav' );
function hide_login_nav()
{
	//?><style>#nav,#backtoblog,.submit,.forgetmenot{display:none}</style><?php
	?><style>#nav,.submit,.forgetmenot{display:none}</style><?php
}


/*
* "Borrowed" from https://wordpress.org/plugins/http-auth
*
* Remove the reauth=1 parameter from the login URL, if applicable. This allows
* us to transparently bypass the mucking about with cookies that happens in
* wp-login.php immediately after wp_signon when a user e.g. navigates directly
* to wp-admin.
*/
add_filter( 'login_url', 'bypass_reauth' );
function bypass_reauth($login_url) {
	$login_url = remove_query_arg('reauth', $login_url);
	return $login_url;
}